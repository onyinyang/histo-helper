package main

import (
	"log"
	"math"
	"math/rand"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func main() {
	temps := prometheus.NewHistogram(prometheus.HistogramOpts{
		Name:    "ratio_seen",
		Buckets: prometheus.LinearBuckets(0.0, 0.1, 30),
		Help:    "The different bandwidth ratios that were observed",
	})

	// Simulate some observations.
	go func() {
		for {
			for i := 0; i < 2000; i++ {
				rand.Seed(time.Now().UnixNano())
				min := -1.0
				max := 5.0
				some_rando := min + rand.Float64()*(max-min)
				histRatio := 0.0
				const maxRatioVal = 3.0
				const minRatioVal = 0.0
				if some_rando <= minRatioVal {
					histRatio = minRatioVal
				} else if some_rando >= maxRatioVal {
					histRatio = maxRatioVal
				} else {
					histRatio = math.Round(some_rando*10) / 10
				}
				temps.Observe(histRatio)
			}
			time.Sleep(100 * time.Second)
		}
	}()
	http.Handle("/metrics", promhttp.Handler())
	log.Fatal(http.ListenAndServe(":8080", nil))
}
