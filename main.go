package main

import (
	"log"
	"math"
	"math/rand"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func main() {
	histogram := prometheus.NewHistogram(prometheus.HistogramOpts{
		Name:    "histogram_metric",
		Help:    "The temperature of the frog pond.",    // Sorry, we can't measure how badly it smells.
		Buckets: prometheus.LinearBuckets(0.0, 0.1, 30), // 30 buckets, each 0.1 mb/s wide.
	})

	// Simulate some observations.
	go func() {
		for {
			for i := 0; i < 2000; i++ {
				rand.Seed(time.Now().UnixNano())
				min := -1.0
				max := 5.0
				some_rando := min + rand.Float64()*(max-min)
				histRatio := 0.0
				const maxRatioVal = 3.0
				const minRatioVal = 0.0
				if some_rando <= minRatioVal {
					histRatio = minRatioVal
				} else if some_rando >= maxRatioVal {
					histRatio = maxRatioVal
				} else {
					histRatio = math.Round(some_rando*10) / 10
				}
				histogram.Observe(histRatio)
			}
			time.Sleep(4 * time.Second)
		}
	}()
	http.Handle("/metrics", promhttp.Handler())
	log.Fatal(http.ListenAndServe(":8080", nil))
}
